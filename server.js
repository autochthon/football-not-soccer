/* -------------------------------------------------------------------------- */
/*                                   IMPORTS                                  */
/* -------------------------------------------------------------------------- */
const express = require("express");
const bodyParser = require("body-parser");
const axios = require("axios");
const logger = require("morgan");
const cors = require("cors");
const path = require("path");
// const fs = require("fs");
require("dotenv").config();

const app = express();
const apiKey = process.env.REACT_APP_API_KEY;
const port = process.env.PORT || 5000;
/* -------------------------------------------------------------------------- */
/*                                 Middlewares                                */
/* -------------------------------------------------------------------------- */

/* --------------------------------- logging data  -------------------------------- */
app.use(logger("dev"));
app.use(cors());

/* ------------------------------ data parsers ------------------------------ */
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: false }));

/* ------------------------ serving static data/pages ----------------------- */
// app.use(express.static(path.join(__dirname, "./client/build")));
// app.use("/api/competitions", competitionsRoutes);

/* ---------------------- environment select/determine ---------------------- */
// if (process.env.NODE_ENV === "production ") {
//   app.use(express.static(path.join(__dirname, "/client/build")));
// }

if (process.env.NODE_ENV === "production") {
  // Serve any static files
  app.use(express.static(path.join(__dirname, "client/build")));

  // Handle React routing, return all requests to React app
  app.get("*", function (req, res) {
    res.sendFile(path.join(__dirname, "client/build", "index.html"));
  });
}

/* -------------------------------------------------------------------------- */
/*                                   ROUTES                                   */
/* -------------------------------------------------------------------------- */

// app.get("/", (req, res, next) => {
//   res.sendFile(path.join(__dirname, "/client/build/index.html"));
// });

app.get("/api", (req, res, next) => {
  const compData = axios
    .get("https://api.football-data.org/v2/competitions/", {
      headers: { "X-Auth-Token": apiKey },
    })
    .then((results) => {
      // console.log(results.data);
      return res.send(results.data);
    })
    .catch((err) => console.error(err));

  // res.json(compData.data);
});

// app.use("/api/competition/:compId", competitionIdRoutes);
// app.use("/api/matches", matchesRoutes);
// app.use("/api/matches/:matchId", matchesIdRoutes);
// app.use("/api/players", playersRoutes);

app.listen(port, function () {
  console.log("Runnning on port: " + port);
});
