# Available Scripts

In the project directory, you can run:

## `npm run dev`

this will concurrently run the server and the client in a console and anotate with color by coatin between them.

### `npm start`

Runs the server app in the development mode.<br />
Open [http://localhost:5000/api](http://localhost:5000/api) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run client-install`

This will install any depedency that is in the `package.json`

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run client`

Runs the `npm start` of the React app in the `client` folder and runs it in [https://localhost:3000](https://localhost:3000), view it in the browser.
