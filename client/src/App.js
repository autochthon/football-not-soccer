import React, { Component } from "react";
import "./App.css";

import axios from "axios";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
    };
  }

  componentDidMount() {
    axios.get("/api").then((response) => {
      const { data: { competitions = [] } = {} } = response;
      this.setState({ items: competitions });
      console.log(competitions);
      // this.setState({ items: response.data.competitions });
      // console.log(response.data.competitions);
      console.log(response.status);
      console.log(response.statusText);
    });
  }
  render() {
    const { items } = this.state;

    return (
      <div className="App">
        {items.map((comp) => (
          <div key={comp.id} className="box">
            <p>
              <span>ID:</span>
              {comp.id}{" "}
            </p>
            <p>
              <span>Name:</span> {comp.name}
            </p>
            <p>
              <span>Plan:</span> {comp.plan}
            </p>
          </div>
        ))}
      </div>
    );
  }
}

export default App;
